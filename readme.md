# Roll20 mofications
A Chrome plugin that makes makes the following modifications to roll20.
1. Displays the weight excluding coin in parentheses
2. Changes the color of successful death saves to green

## Installation
1. Go to chrome://extensions/
2. Select load unpacked
3. Select the folder with the extension