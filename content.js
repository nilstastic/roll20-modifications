// fixa lyckade death saves färgen
const css = document.createElement("style");
css.innerHTML = "span[data-i18n='successes-u'] ~ input:checked { background-color:lightgreen!important;}";
document.head.appendChild(css);

var interval = window.setInterval(p => {

    // fixa så att man kan se hur mycket man bär på sig utan pengar
    if(document.querySelector("span[data-i18n='gold-piece-u']") !== null) {
       
        const platinum = document.getElementsByName("attr_pp")[0].value;
        const gold = document.getElementsByName("attr_gp")[0].value;
        const silver = document.getElementsByName("attr_sp")[0].value;
        const ep = document.getElementsByName("attr_ep")[0].value;

        const totalCoinWeight = 0.020625 * (parseInt(platinum) + parseInt(gold) + parseInt(silver) + parseInt(ep));
        const totalWeightNode = document.getElementsByName("attr_weighttotal")[0];
        totalWeightNode.value = `${parseFloat(totalWeightNode.value)} (${Math.round((parseFloat(totalWeightNode.value) - totalCoinWeight) * 10)/10})`
    
    }
}, 1000);
